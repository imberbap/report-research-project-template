"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""
from pathlib import Path
import matplotlib.pyplot as plt
import sys
from fluidsim.solvers.models0d.lorenz.solver import Simul

here = Path(__file__).absolute().parent
path_dir_save = Path.joinpath(here, 'fig')   # permet d'envoyer les figures dans le dossier


params = Simul.create_default_params()

dt=0.00001
params.time_stepping.deltat0 = dt
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
fig=plt.gcf()
# see also the other plot_* methods of sim.output.print_stdout


if "SAVE" in sys.argv:
    fig.savefig(path_dir_save / f"fig_job_lorenz_{dt}.png")
else:
    plt.show()
