\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage{hyperref}
\usepackage{physics}
\usepackage{hyperref}


\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.4\linewidth]{../fig/logo_phitem}
\vspace{2cm}

\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}


\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Report Instabilities and Turbulences}
\author{Baptiste IMBERT}


% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}



% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}


\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\tableofcontents

\newpage

\section{Introduction}
\textrm{This report is intended to be understood by anyone. Whether you are part of the scientific world or not. As a first step, this outline will explain a mathematical model, the predator–prey model.}
\newline
\textrm{How was it discovered, how can it be explained and what are its areas of application? These are the questions addressed here.}
\newline
\textrm{In a second step, an extremely well-known experiment in fluid mechanics, the creation of a boundary layer on a surface. An application that is present everywhere in our world, whether in aeronautics with wing profiles or in meteorology with the earth's atmosphere and its surface.}

\section{The predator-prey model}

\subsection{Behind this model}
\textrm{Also known as  the Lotka–Volterra equations, this model determines a relationship between the number of predators and prey .}
\newline
\advice{"Predator-prey models are arguably the building blocks of the bio- and ecosystems as biomasses are grown out of their resource masses. Species compete, evolve and disperse simply for the purpose of seeking resources to sustain their struggle for their very existence. Depending on their specific settings of applications, they can take the forms of resource-consumer, plant-herbivore, parasite-host, tumor cells (virus)-immune system, susceptible-infectious interactions, etc. They deal with the general loss-win interactions and hence may have applications outside of ecosystems. When seemingly competitive interactions are carefully examined, they are often in fact some forms of predator-prey interaction in disguise."}
\textrm{Dr. Frank Hoppensteadt, Courant Institute of Mathematical Sciences, NYU, New York, NY}

\subsection{History}
\textrm{The first person to take a scientific look at population dynamics was Leonardo Fibonacci. In his work}
\advice{Liber abaci (1202)}
\textrm{, he proposed a sequence of numbers giving an answer to a problem of population multiplication. }
\newline
\textrm{However, it was not until 1798 that we had the modern foundations of our model. Thomas Robert Malthus considers an ideal population composed of a single homogeneous animal species. Variations in age, size and possible periodicity of births and deaths are neglected. The species lives alone in an unchanging environment or coexists with other species without direct or indirect influence.}
\newline
\textrm{In his statement}
\advice{"Population, when unchecked, increases in a geometrical ratio".}
\textrm{It assumes that the rate of increase in the number of individuals in a population over a period of time is proportional to the number of individuals. Related by a constant factor called the coefficient of increase or growth rate. }
\subsection{Theory}

\subsubsection{The equations }
\textrm{We consider the predator–prey model:}
\begin{align}
\dot X &= AX-BXY \\
\dot Y &= -CY+DXY
\end{align}

\raggedright
\textrm{X(t) and Y(t) are two functions of time representing species of preys and predators,
respectively. A,B,C and D are real positive parameters.} 

\begin{itemize}
    \item A, is the intrinsic reproduction rate of prey(constant, independent of the number of predators)
    \item B, the mortality rate of prey due to predators encountered
    \item C, intrinsic mortality rate of predators (constant, independent of the number of prey)
    \item D, reproductive rate of predators in relation to prey encountered and eaten
\end{itemize}


\subsubsection{Fixed points}
\textrm{The fixed points are the values of X and Y such that ((X',Y')=(0,0)).}
\newline
\newline
\textrm{Equations (1) and (2) can be factored:}
\begin{align}
\dot X&= X(A-BY) \\
\dot Y&= -Y(C-DX)
\end{align}
\textrm{One solution is direct, (X,Y)=(0,0), which simply corresponds to the extinction of the two species, this is called a trivial solution.}
\newline
\textrm{We see that there is also a non-trivial fixed point (X,Y)=($\frac{C}{D},\frac{A}{B}$).}

\subsubsection{Stability of the fixed points}

\textrm{We will now study the stability of these points.}
\newline
\newline
\textrm{We consider an infinitesimal perturbation which is also constant as:}
\begin{align}
X &= X_f+x \\
Y &= Y_f+y
\end{align}
\textrm{Expressions (5) and (6) are fed back into equations (3) and (4) respectively.}
\newline
\textrm{After linearisation we obtain the following expressions for}
\textbf{the first fixed point}
\textrm{(X,Y)=(0,0):}
\begin{align}
\dot x&= Ax \\
\dot y&= -Cy
\end{align}
\textrm{We pose the vectors:}

$$V=\begin{pmatrix}
x \\
y
\end{pmatrix},
\dot V=\begin{pmatrix}
\dot x \\
\dot y
\end{pmatrix}$$

\textrm{The equations (7) and (8) can be written in matrix form:}

$$\dot V=\begin{pmatrix}
A & 0 \\
0 & -C
\end{pmatrix}V$$

\textrm{With:}

$$J=\begin{pmatrix}
A & 0 \\
0 & -C
\end{pmatrix}$$
\newline

\textrm{J is the Jacobian matrix of the system, this matrix contains the partial derivatives, its name comes from the mathematician Charles Jacobi.}
\newline
\newline
\textrm{We need to find eigenvalues and eigenmodes of this matrix.}
\newline
\textrm{In this case it is very simple because the matrix J is already diagonal, the eigenvalues are:}
\begin{align}
\lambda_1 &= A \\
\lambda_2 &= -C
\end{align}

\textrm{By deduction the eigenmodes are:}

$$V_1=\begin{pmatrix}
1  \\
0
\end{pmatrix},
V_2=\begin{pmatrix}
0  \\
1
\end{pmatrix}$$     
 \textrm{In our case, we said that A and C were positive. Which means that $\lambda_1$ is positive and $\lambda_2$ is negative.}
\newline
\textrm{This fixed point is unstable, it's a saddle point.}
\newline
\newline
\textrm{Now from}
\textbf{the second fixed point}
\textrm{(X,Y)=($\frac{C}{D},\frac{A}{B}$):}

$$\dot V=\begin{pmatrix}
0 & -\frac{CB}{D} \\
\frac{AD}{B} & 0
\end{pmatrix}V$$

\textrm{With this time:}

$$J=\begin{pmatrix}
0 & -\frac{CB}{D} \\
\frac{AD}{B} & 0
\end{pmatrix}$$

\textrm{Unfortunately J is not diagonal which complicates the determination of the eigenvalues. }
\newline
\textrm{The equation we have to solve is:}
\begin{align}
det(J-\lambda I=0)
\end{align}

\textrm{With I the identity matrix:}

$$I=\begin{pmatrix}
1 & 0 \\
0 & 1
\end{pmatrix}$$

\textrm{The eigenvalues are:}
\begin{align}
\lambda_1 &= -i\sqrt{AC} \\
\lambda_2 &= i\sqrt{AC}
\end{align}

\textrm{The fixed point is elliptic, this describes the shape of the orbit near the point.}
\newline
\newline
\textrm{To find the eigenvectors, we reinject the eigenvalues (12) and (13) into equation (11) and after simplification we obtain the eigenvectors:.}

$$V_1=\begin{pmatrix}
-i\frac{B}{D}\sqrt{\frac{C}{A}}  \\
1
\end{pmatrix},
V_2=\begin{pmatrix}
i\frac{B}{D}\sqrt{\frac{C}{A}}   \\
1
\end{pmatrix}$$  

\subsection{Simulations}

\subsubsection{Program}
\textrm{The program works on Python with a special package called}
\advice{FluidDym}
\textrm{developed by Pierre Augier, a researcher at LEGI studying geophysical turbulence with experiments and numerical simulations at University of Grenoble Alpes.}
\newline
\textrm{In this package, we are using}
\advice{Fluidsim}
\textrm{created especially for numerical simulations. The parameters are a time of 20 seconds and a step of 0.1 seconds, the graphs can be saved directly with the addition of "SAVE" when starting the program. }


\subsubsection{Figures}
\textrm{Here are the graphs obtained after running the program, two simulations were performed with different input parameters.}

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/fig_job_predapey1.png}
\newline
{Figure 1 : Y the predators as a function of X prey (a=2,b=1)}
\label{Y the predators as a function of X prey}
\newline

\raggedright

\textrm{This graphic shows the important relation between preys and predators. On the bottom of the curve, we can see the number of preys increase while the predators are approximately constant.}
\newline
\textrm{However, as predators increase the number of prey decreases as a consequence.}
\newline
\textrm{In the same way, at the top of the graph, when the preys start to decrease, the predators are constant and when the predators become constant the prey increases.}

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/fig_job_predapey2.png}
\newline
{Figure 2 : Y the predators and the X the prey as function of time (a=2,b=1)}
\newline

\raggedright

\textrm{The first maximum of predators is reached very quickly for a time of about 0.5 seconds. However, the prey curve keeps going down until 2 seconds.}
\newline
\textrm{Predators drop to 5 seconds, but they are rising in response to increased prey with a difference of about 3 seconds.}
\newline
\textrm{This difference may represent years in a real model with an increase in predator reproduction due to increased food supply.}
\newline
\textrm{By the same logic, when prey decreases at t=6 s, in response predators decrease at a time of 8 seconds.}
\newline
\newline
\textrm{Then the cycle is periodic and repeats itself, like the history of life.}

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/fig_job_predapey1_2.png}
\newline
{Figure 3 : Y the predators as a function of X prey (a=1,b=0.5)}
\label{Y the predators as a function of X prey}
\newline

\raggedright

\textrm{The behaviour of the curve is exactly the same as in figure (1). However, the values are different.}

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/fig_job_predapey2_2.png}
\newline
{Figure 4 : Y the predators and the X the prey as function of time (a=1,b=0.5)}
\newline

\raggedright

\textrm{The description is the same as graph 2 with the same oscillations, we will focus instead on the influence of the input parameters.}
\newline
\textrm{Fisrt of all, an oscillation has been added which means that the phenomenon is faster than before.}
\newline
\textrm{Moreover, the maximum and minimum have changed, the number of prey and predators are different.}



\section{Boundary layer on a surface in a turbulent flow}
\textrm{Boundary layers are a common phenomenon around us as mentioned in the introduction.}
\newline

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/simulation.png}
\newline
{Figure 5 : Boundary layer on a wing profil}
\newline

\raggedright

\textrm{The latter may be everywhere but is still very complex. This layer will define a thickness where the flow will become turbulent, with a solid in y=0 as reference. However, near this wall in the boundary layer, the flow becomes laminar.}
\newline
\textrm{For example, in Figure 5, the creation of the boundary layer can be seen in blue with the significant creation of vortices upstream of the wing. }
\newline
\newline
\textrm{To describe its behaviour, powerful fluid mechanics equations must be used, the conservation of momentum equation, known as the}
\advice{Navier-Stockes equation.}

\begin{equation}
\rho(\frac{\partial \overrightarrow{u}}{\partial t}+\overrightarrow{u}(\overrightarrow{\nabla}.\overrightarrow{u}))=f+\overrightarrow{\nabla} P+\mu \Delta \overrightarrow{u}
\end{equation}

\begin{itemize}
    \item $\rho$, the density $(Kg/m^3)$
    \item $\mu$, dynamic viscosity  (Pa.s)
    \item u, the speed vector (u,v,w)
    \item f, the external forces (N)
    \item P, the pressure (Pa)
    \item $\nabla$, Nabla operator 
    \item $\Delta$, Laplacien operator 

	

\end{itemize}

\textrm{However, to describe this turbulent flow another equation derived from}
\advice{Navier and Stockes equation}
\textrm{will be used}
\advice{The Averaged Navier Stockes equations (RANS):}

\begin{equation}
\rho(\frac{\partial \overline{u}}{\partial t}+\overline{u}(\nabla.\overline{u}))=\nabla \overline{P}+\mu \Delta \overline{u}
\end{equation}

\advice{(Without external forces)}
\newline

\textrm{The idea behind the equations is Reynolds decomposition, whereby an instantaneous quantity is decomposed into its time-averaged and fluctuating quantities as :}
\begin{equation}
u=\overline{u}+u'
\end{equation}
\begin{itemize}
    \item $\overline{u}$, Average of u
    \item $u'$, Fluctuation of u

\end{itemize}


\textrm{To simplify the calculations, the case that will be studied is the following:}

\centering

\includegraphics[width=0.7\linewidth]{../Pysimul/fig/schema}
\newline
{Figure 6 : Boundary layer on a plate in 2D}
\newline

\raggedright

\textrm{No component and invariant to z,it is assumed that the velocity according to Ux is very large compared to the velocity Vy and that the derivatives according to y are very large compared to those of x.}
\newline
\textrm{The flow is considered incompressible.}
\textrm{After simplification, the equation according to the velocity U is:}

\begin{equation}
\overline{u}\frac{\partial \overline{u}}{\partial x}+\overline{v}\frac{\partial \overline{u}}{\partial y}=\nu \pdv[2]{\overline u}{y}-\pdv{\overline{u'v'}}{y}
\end{equation}

\textrm{$\overline{u'v'}$ is the tensor of Reynolds, it is equal to $\overline{u'v'}=\nu_{t}(x,y)\pdv{\overline{u}}{y}$.} 
\newline
\textrm{$\nu_{t}(x,y)$ is the turbulent viscosity.}
\newline
\newline
\textrm{The conservation of mass equation is:}
\begin{equation}
\frac{\partial \overline{u}}{\partial x}+\frac{\partial \overline{v}}{\partial y}=0
\end{equation}

\textrm{A similar self-similar solution is sought for the longitudinal velocity of the form
$\overline{u}(x,y)=U_{o}f(\eta)$ with $\eta=\frac{y}{\delta(x)}$.} 
\newline
\textrm{A current function is introduced as $\psi(x,y)=U_o\delta(x)F(\eta)$ with $F'(\eta)=f(\eta)$.}
\newline
\newline
\textrm{Thanks to the equations $\overline{u}=\pdv{\psi}{y}$ and $\overline{v}=-\pdv{\psi}{x}$, $\overline{u}$ and $\overline{v}$ are know and can be reinjected into (17).}
\newline
\newline
\textrm{This differential equation is then obtained :}
\begin{equation}
F'''+\frac{1}{\nu}\frac{(\partial \nu_{t}F'')}{\partial \eta}+\frac{U_{o}\delta'\delta}{\nu}FF''=0
\end{equation}

\textrm{Two possibilities for modelling turbulent viscosity are presented:}
\begin{itemize}
    \item $l_m=b\delta(x)$ and $\nu_{t}=U_{o}l_{m}$ based on global data outside the boundary layer.
    \item $l_m=\kappa y$ and $\nu_{t}=l_{m}^{2}\pdv{\overline{u}}{y}$ based on local data inside the boundary layer.
\end{itemize}

\textrm{With $l_{m}$ the mixing length and $U_{\tau}$ the speed of friction.}

\centering
\includegraphics[width=1\linewidth]{../Pysimul/fig/Pope2}
{Figure 7 : Graphic from Turbulent Flows by Pope}
\newline

\raggedright

\textrm{In the start area (1), $\frac{\nu}{u_{\tau}\delta}\approx \frac{l_{m}}{\delta}$, so $\nu_{t}\approx l_{m}u_{\tau}$ and $l_{m}\propto y$.}
\newline
\newline
\textrm{In the second area (2), $l_{m}=cst$, $l_{m}\approx 0,12\delta$, the second possibility of modelling the turbulent viscosity listed above is find.}
\newline
\newline
\textrm{For the third area (3), we can assume that $\nu=0$, we can solve (19) and after simplification and normalisation we find the solution :}

\begin{equation}
u^{+}=\frac{1}{\kappa}ln(y^{+})+B
\end{equation}

\begin{itemize}
    \item $\kappa$=0.41, it's name is constant of Karman 
    \item B=5.2
\end{itemize}

\textrm{In the literature, there is some variation in the values ascribed to the log-law
constants, but generally they are within $5\%$.}

\centering
\includegraphics[width=1\linewidth]{../Pysimul/fig/Pope1}
{Figure 8 : Graphic from Turbulent Flows by Pope with the description above}
\newline
\raggedright

\textrm{In figure 8, on the x-axis we have y divided by  $\delta_{\nu}$ with a logarithmic scale. For the y-axis, the velocity u is normalised by $U_{\tau}$.}
\newline
\newline
\textrm{With the logarithmic scale, if the curve is straight it means that it follows a logarithmic law. As we can see for $y^{+}>100$, the curve is straight for the simulation (dashed line). We find the solution (20).}
\newline
\newline
\textrm{However, when we are close to the surface ($y^{+}<10$), we have a curve which isn't straight, that means without the logarithmic scale we have something linear.}
\newline 
\textrm{This is what we found with the figure 7 and some hypotheses.}
\newline
\newline
\textrm{The most difficult part to describe remains the in-between for $10<y^{+}<100$.}
\newline
\newline
\textrm{A possible model for the whole boundary layer is:}
\begin{equation}
\frac{\overline{u}}{u_{\tau}}=\frac{1}{\kappa}log(\frac{y}{\delta_{\nu}})+\frac{\Pi}{\kappa}\omega(\frac{y}{\delta})
\end{equation}

\textrm{Where $u_{\tau}$ is the friction velocity, $\kappa$ the von Karman constant, $\delta_\nu$ the viscous scale, $\Pi$ a constant. $\omega$ is a function approximated by:}

\begin{equation}
\omega(\frac{y}{\delta})=2sin^2(\frac{\pi y}{2\delta})
\end{equation}

\textrm{If we look again at the figure 8, we can see that the experiments of Klebanoff (circles) is really close to the simulation.}
\newline
\textrm{More interesting, the Reynolds number has no effect on the beginning of the law but we can see on the experimental curves a plateau for large values of $y^+$. It comes from the fact that the measuring tool leaves the wake produced by the upstream object, i.e. we leave the turbulent flow and enter a laminar zone where the speed is constant.}



\section{Bibliography}
\advice{Turbulent Flows}
\textrm{by}
\advice{Stephen B. Pope}
\textrm{(2000)}
\newline
\url{https://www.cambridge.org/fr/academic/subjects/physics/nonlinear-science-and-fluid-dynamics/turbulent-flows?format=PB&isbn=9780521598866}
\newline
\newline
\advice{Predator-Prey Model}
\textrm{by}
\advice{Frank Hoppensteadt}
\textrm{(2006)}
\newline
\url{http://www.scholarpedia.org/article/Prey-predator}
\newline
\newline
\advice{Le paradoxe du modèle prédateur-proie de Vito Volterra}
\textrm{by}
\advice{Jean-Marc Ginoux}
\textrm{(2006)}
\newline
\url{http://ginoux.univ-tln.fr/HDS/Le$\%20paradoxe\%20du\%20mod\%E8le\%20pr\%E9dateur-proie\%20de\%20Vito\%20Volterra.pdf}
\newline
\newline
\advice{Turbulent fluid dynamics tutorials of Master 1 SIM}
\textrm{by}
\advice{Nicolas Mordant}
\textrm{(2022)}
\newline
\newline
\newline
\newline
\newline
\newline
\centerline{\advice{End}}


\end{document}
